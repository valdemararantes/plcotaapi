/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
//@RunWith(CdiRunner.class)
//@AdditionalClasses(value = {Logger.class, BeanManager.class})
//@RunWith(WeldJUnit4Runner.class)
//@Named
public class TimeIntervalTest {

    private Logger log = LoggerFactory.getLogger(TimeIntervalTest.class);

    public TimeIntervalTest() {
    }

    @Test
    public void a() {
        try {
            log.trace("Iniciando teste");

            TimeInterval timeInterval = new TimeInterval(1, 100);
//            Weld weld = new Weld();
//            WeldContainer weldContainer = weld.initialize();
//            TimeInterval timeInterval = weldContainer.instance().select(TimeInterval.class).get();
            timeInterval.setSplitDuration(1);

            int counter = 0;
            while(timeInterval.hasNext()) {
                TimeInterval nextInterval = timeInterval.next();
                log.trace("{}: {} - {}", ++counter, nextInterval.getStartTime(), nextInterval.getEndTime());
            }
        } catch (Exception e) {
            log.error(null, e);
        } finally {
            log.trace("Fim do teste");
        }
    }

    //@Produces
    public TimeInterval getTimeInterval() {
        log.trace("CDI TimeInterval Producer de Teste");
        return new TimeInterval(1, 100);
    }
//
//    @Produces
//    Logger getLogger(InjectionPoint injectionPoint) {
//        return LoggerFactory.getLogger(injectionPoint
//                .getMember().getDeclaringClass()
//                .getName());
//    }
//
//    public void testJoda() {
//        log.trace("Iniciando teste");
//        try {
//
//            // Instanciando Calendar e setando para 1 hora antes de agora
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.HOUR, -1);
//
//            DateTime startDateTime = new DateTime(cal.getTimeInMillis());
//            log.trace("startDateTime={}", startDateTime.getMillis());
//
//            // Marcando objeto calendar para 5 horas antes de startTime
//            cal.add(Calendar.HOUR, -5);
//
//            DateTime endDateTime = new DateTime(cal.getTimeInMillis());
//            log.trace("endtDateTime={}", endDateTime);
//
//            // Convertendo para Gregoriano
//            GregorianCalendar gregCal = new GregorianCalendar();
//            gregCal.setTimeInMillis(startDateTime.getMillis());
//            log.trace("gregCal={}", gregCal.getTimeInMillis());
//
//
//
//        } finally {
//            log.trace("Fim do teste");
//        }
//    }
}
