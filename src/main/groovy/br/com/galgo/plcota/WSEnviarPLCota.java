/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota;

import br.com.galgo.galgowsgenerated.GalgoServicesFactory;
import br.com.galgo.plcota.model.PLCotaPlain;
import br.com.stianbid.common.MessageReceiptComplexType;
import br.com.stianbid.schemaplcota.MessageEnviarPLCotaComplexType;
import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType;
import br.com.stianbid.serviceplcota.ConsumirFaultMsg;
import br.com.stianbid.serviceplcota.EnviarFaultMsg;
import br.com.stianbid.serviceplcota.ServicePLCota;
import com.galgo.cxfutils.CXFUtils;
import com.galgo.cxfutils.sec.User;
import com.galgo.cxfutils.sec.WSHeader;
import com.galgo.cxfutils.ws.WS;
import com.galgo.utils.ApplicationException;
import com.galgo.utils.Cronometro;
import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import com.galgo.utils.cert_digital.TrustAllCerts;
import iso.std.iso._20022.tech.xsd.reda_001_001.DateAndDateTimeChoice;
import iso.std.iso._20022.tech.xsd.reda_001_001.PriceValuation3;
import iso.std.iso._20022.tech.xsd.reda_001_001.ValuationTiming1Code;
import jodd.bean.BeanUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXB;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;

/**
 * @author valdemar.arantes
 */
public class WSEnviarPLCota extends WS {

    public static final String PC_0137 = "PC.0137";
    public static final String PC_0237 = "PC.0237";
    private static final String SERVICE_PLCOTA = "/ServicePLCota";
    private static final Logger log = LoggerFactory.getLogger(WSEnviarPLCota.class);
    private static final Logger logXML = LoggerFactory.getLogger(WSEnviarPLCota.class.getName() + ".XML");
    private static ServicePLCota servicePLCota;
    private String login;
    private String pwd;
    private MessageRetornoPLCotaComplexType ret;

    /**
     * Recupera o ambiente (homologação ou produção) e guarda em um campo
     */
    public WSEnviarPLCota() {
        // Aceitando todos os certificados SSL
        TrustAllCerts.doIt();
    }

    public MessageReceiptComplexType enviar(List<PLCotaPlain> plCotaList) throws ConsumirFaultMsg {
        instantiateWS();

        if (CollectionUtils.isEmpty(plCotaList)) {
            log.info("Lista de cotas vaiza. Retornando..");
            return null;
        }

        WSHeader.buildHeader(servicePLCota, new User(login, pwd));

        StringWriter sWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(sWriter);
        CXFUtils.turnOnClientLoggingFeature(getServicePLCota(), writer);

        MessageEnviarPLCotaComplexType req = new MessageEnviarPLCotaComplexType();

        BeanUtil.setPropertyForcedSilent(req, "pricRptV04.msgId.id", "java_code_by_Valdemar");

        //XMLGregorianCalendar dt = req.getPricRptV04().getMsgId().getCreDtTm();
        BeanUtil.setPropertyForcedSilent(req, "pricRptV04.msgId.creDtTm", XMLGregorianCalendarConversionUtil
                .asXMLGregorianCalendar(new Date()));

        // Paginação (Uma página)
        //boolean b = req.getPricRptV04().getMsgPgntn().isLastPgInd();
        //String pn = req.getPricRptV04().getMsgPgntn().getPgNb();
        BeanUtil.setPropertyForcedSilent(req, "pricRptV04.msgPgntn.lastPgInd", true);
        BeanUtil.setPropertyForcedSilent(req, "pricRptV04.msgPgntn.pgNb", 1);

        int counter = 0;
        for (PLCotaPlain plcota : plCotaList) {
            //String b = req.getPricRptV04().getPricValtnDtls().get(0).getId();
            BeanUtil.setPropertyForcedSilent(req, "pricRptV04.pricValtnDtls[" + counter + "].id", "PLCota_" + counter);
            PriceValuation3 pricValtnDtls = req.getPricRptV04().getPricValtnDtls().get(counter);

            // NAVDtTm
            pricValtnDtls.setNAVDtTm(new DateAndDateTimeChoice());
            pricValtnDtls.getNAVDtTm().setDt(XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(plcota
                    .getData()));

            // FinInstrmDtls
            //String o = pricValtnDtls.getFinInstrmDtls().getId().get(0).getOthrPrtryId().getId();
            //String o = pricValtnDtls.getFinInstrmDtls().getId().get(0).getOthrPrtryId().getPrtryIdSrc();
            BeanUtil.setPropertyForced(pricValtnDtls, "finInstrmDtls.id[0].othrPrtryId.id", plcota.getCdSti());
            BeanUtil.setPropertyForced(pricValtnDtls, "finInstrmDtls.id[0].othrPrtryId.prtryIdSrc", "SistemaGalgo");

            // DnmtnCcy
            pricValtnDtls.getFinInstrmDtls().setDnmtnCcy("BRL");

            // DualFndInd
            pricValtnDtls.getFinInstrmDtls().setDualFndInd(false);

            // SplmtryId - Número de cotistas (Info. ANBIMA)
            Integer qtdCotistatas = plcota.getQtdCotistas();
            if (qtdCotistatas != null) {
                pricValtnDtls.getFinInstrmDtls().setSplmtryId(qtdCotistatas.toString());
            }

            // TtlNAV (PL)
            //BigDecimal o = pricValtnDtls.getTtlNAV().get(0).getValue();
            //String o = pricValtnDtls.getTtlNAV().get(0).getCcy();
            BeanUtil.setPropertyForced(pricValtnDtls, "ttlNAV[0].value", plcota.getPl());
            BeanUtil.setPropertyForced(pricValtnDtls, "ttlNAV[0].ccy", "BRL");

            // NAV (Valor da cota)
            //BigDecimal o = pricValtnDtls.getNAV().get(0).getUnit();
            BeanUtil.setPropertyForced(pricValtnDtls, "NAV[0].unit", plcota.getCota());

            // TtlUnitsNb (Qtd. de cotas)
            BeanUtil.setPropertyForced(pricValtnDtls, "ttlUnitsNb.unit", plcota.getQtdCotas());

            // ValtnTp
            pricValtnDtls.setValtnTp(ValuationTiming1Code.USUA);

            // InvstNetAmt - Aplicação (Info. ANBIMA) (Moeda em Reais)
            //BigDecimal pricValtnDtls.getInvstNetAmt().getValue();
            //String pricValtnDtls.getInvstNetAmt().getCcy()
            if (plcota.getAplicacao() != null) {
                BeanUtil.setPropertyForced(pricValtnDtls, "invstNetAmt.value", plcota.getAplicacao());
                BeanUtil.setPropertyForced(pricValtnDtls, "invstNetAmt.ccy", "BRL");
            }

            // RedNetAmt - Resgate (Info. ANBIMA)
            //pricValtnDtls.getRedNetAmt().getValue();
            //pricValtnDtls.getRedNetAmt().getCcy();
            if (plcota.getResgate() != null) {
                BeanUtil.setPropertyForced(pricValtnDtls, "redNetAmt.value", plcota.getResgate());
                BeanUtil.setPropertyForced(pricValtnDtls, "redNetAmt.ccy", "BRL");
            }

            // LclTaxAmt - Come Cotas (Info. ANBIMA)
            //BigDecimal pricValtnDtls.getLclTaxAmt().getValue()
            if (plcota.getComeCotas() != null) {
                BeanUtil.setPropertyForced(pricValtnDtls, "lclTaxAmt.value", plcota.getComeCotas());
                BeanUtil.setPropertyForced(pricValtnDtls, "lclTaxAmt.ccy", "BRL");
            }

            // OffclValtnInd
            pricValtnDtls.setOffclValtnInd(true);

            // SspdInd
            pricValtnDtls.setSspdInd(false);

            // AddtlInf - Quando for reenvio
            if (plcota.isReenvio()) {
                BeanUtil.setPropertyForced(pricValtnDtls, "addtlInf[0]", "REENVIO INFORMACAO");
            }

            counter++;
        }

        Writer xml = new StringWriter();
        JAXB.marshal(req, xml);
        log.debug("request=\n{}", xml.toString());

        StringWriter msg = new StringWriter();
        MessageReceiptComplexType receipt = enviar_(req, sWriter, msg);
        xml = new StringWriter();
        JAXB.marshal(receipt, xml);
        log.debug("response=\n{}", xml.toString());

        return receipt;
    }

    public MessageRetornoPLCotaComplexType getResponse() {
        return ret;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    protected String getWsUriName() {
        return SERVICE_PLCOTA;
    }

    /**
     * Invoca a operação Consumir do Web Service PLCota. Se ocorrer erro diferente de PC.0137 ou
     * PC.0237, tenta até 3 vezes.
     *
     * @param req     Request
     * @param sWriter StringWriter utilizado pelo log do CXF
     * @param msg     StringWriter que recebe o tempo gasto para a execução do Web Service
     */
    private MessageReceiptComplexType enviar_(MessageEnviarPLCotaComplexType req, StringWriter sWriter, StringWriter
            msg) {
        log.info("REQUEST: {}", this);
        MessageReceiptComplexType receipt = null;
        msg.getBuffer().setLength(0);
        Cronometro cron = Cronometro.start();
        try {
            log.info("Invocando o Web Service...");
            receipt = servicePLCota.enviar(req);
            return receipt;
        } catch (EnviarFaultMsg | SOAPFaultException e) {
            log.error(null, e);
            if (!PC_0137.equals(e.getMessage())) {
                log.error("{}\n}{}", e.getMessage(), sWriter.getBuffer());
            } else if (PC_0237.equals(e.getMessage())) {
                log.warn("Senha inválida!!! {}: {}", e.getMessage());
            } else {
                log.info(e.getMessage());
            }
            throw new ApplicationException(e);
        } catch (Exception e) {
            log.error(null, e);
            throw new ApplicationException(e);
        } finally {
            String elapsedTime = cron.stop().toString();
            log.info("Operação realizada em {}", elapsedTime);
            msg.append(elapsedTime);
        }
    }

    /**
     * Instancia a classe ServiceANBID
     */
    private ServicePLCota getServicePLCota() {
        if (servicePLCota == null) {
            servicePLCota = GalgoServicesFactory.newPLCotaServiceInstance();
            setEndPoint((BindingProvider) servicePLCota);
        }
        return servicePLCota;
    }

    private void instantiateWS() {
        servicePLCota = GalgoServicesFactory.newPLCotaServiceInstance();
        setEndPoint((BindingProvider) servicePLCota);
    }

}
