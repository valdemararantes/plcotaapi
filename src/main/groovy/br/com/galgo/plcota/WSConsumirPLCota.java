/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota;

import br.com.galgo.galgowsgenerated.GalgoServicesFactory;
import br.com.galgo.plcota.utils.Atuacao;
import br.com.galgo.plcota.utils.ConsumidorCallback;
import br.com.galgo.plcota.utils.MessageConsultaPLCotaComplexTypeBuilder;
import br.com.stianbid.schemaplcota.MessageConsultaPLCotaComplexType;
import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType;
import br.com.stianbid.serviceplcota.ConsumirFaultMsg;
import br.com.stianbid.serviceplcota.ServicePLCota;
import com.galgo.cxfutils.CXFUtils;
import com.galgo.cxfutils.sec.WSHeader;
import com.galgo.cxfutils.ws.WS;
import com.galgo.utils.ApplicationException;
import com.galgo.utils.Cronometro;
import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author valdemar.arantes
 */
public class WSConsumirPLCota extends WS {

    private static final String SERVICE_PLCOTA = "/ServicePLCota";
    private static final String MSG_SENDER = "PLCota_GlgApp";
    private static final Logger log = LoggerFactory.getLogger(WSConsumirPLCota.class);
    private static final Logger logXML = LoggerFactory.getLogger(WSConsumirPLCota.class.getName() + ".XML");
    public static final Integer STATUS_ENVIADA = 1;
    public static final Integer STATUS_REENVIADA = 2;
    public static final Integer STATUS_CANCELADA_MAN = 3;
    public static final Integer STATUS_CANCELADA_AUT = 4;
    public static final Integer STATUS_REJEITADA = 5;
    public static final String PC_0137 = "PC.0137";
    public static final String PC_0237 = "PC.0237";
    private static ServicePLCota servicePLCota;
    private XMLGregorianCalendar dtFinEnvio;
    private XMLGregorianCalendar dtInitEnvio;
    private XMLGregorianCalendar dtFinInfo;
    private XMLGregorianCalendar dtInitInfo;
    private Integer codSTI;
    private List<Integer> codSTIList;
    private Integer qtMaxElement;
    private Integer firstPage;
    private Integer lastPage;
    private boolean downloadAllRecords = false;
    private int page = 1;
    private Integer status;
    private Atuacao atuacao;
    private WSListener listener;
    private MessageRetornoPLCotaComplexType ret;
    private int receiveTimeout = 300_000;
    private ConsumidorCallback callback;

    /**
     * Recupera o ambiente (homologação ou produção) e guarda em um campo
     */
    public WSConsumirPLCota() {
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param qtMaxElement
     */
    public WSConsumirPLCota(Integer codSTI, XMLGregorianCalendar dtInitInfo, XMLGregorianCalendar dtFinInfo,
        Integer qtMaxElement) {
        this();
        this.codSTI = codSTI;
        this.dtInitInfo = dtInitInfo;
        this.dtFinInfo = dtFinInfo;
        this.qtMaxElement = qtMaxElement;
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param dtInitEnvio
     * @param dtFinEnvio
     * @param qtMaxElement
     * @param firstPage
     * @param lastPage
     */
    public WSConsumirPLCota(Integer codSTI, Date dtInitInfo, Date dtFinInfo, Date dtInitEnvio, Date dtFinEnvio,
        Integer qtMaxElement, Integer firstPage, Integer lastPage) {
        this();
        this.codSTI = codSTI;
        setDtInitInfo(dtInitInfo);
        setDtFinInfo(dtFinInfo);
        setDtInitEnvio(dtInitEnvio);
        setDtFinEnvio(dtFinEnvio);
        log.info("codSTI={}, dtInitEnvio={}, dtFinEnvio={}, dtInitInfo={}, dtFinInfo={}",
            new Object[]{codSTI, dtInitEnvio, dtFinEnvio, dtInitInfo, dtFinInfo});
        this.qtMaxElement = qtMaxElement;
        this.firstPage = firstPage;
        this.lastPage = lastPage;
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param dtInitEnvio
     * @param dtFinEnvio
     * @param qtMaxElement
     * @param firstPage
     * @param lastPage
     * @param status       Procure utilizar as constantes definidas nesta classe
     */
    public WSConsumirPLCota(Integer codSTI, Date dtInitInfo, Date dtFinInfo, Date dtInitEnvio, Date dtFinEnvio,
        Integer qtMaxElement, Integer firstPage, Integer lastPage, Integer status) {
        this(codSTI, dtInitInfo, dtFinInfo, dtInitEnvio, dtFinEnvio, qtMaxElement, firstPage, lastPage);
        this.status = status;
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param qtMaxElement
     */
    public WSConsumirPLCota(Integer codSTI, Date dtInitInfo, Date dtFinInfo, Integer qtMaxElement) {
        this(codSTI, XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtInitInfo),
            dtFinInfo == null ? null : XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtFinInfo),
            qtMaxElement);
    }

    /**
     * @return Código STI do Fundo de Investimento
     */
    public Integer getCodSTI() {
        return codSTI;
    }

    /**
     * @param codSTI Código STI do Fundo de Investimento
     */
    public void setCodSTI(Integer codSTI) {
        this.codSTI = codSTI;
    }

    public XMLGregorianCalendar getDtFinEnvio() {
        return dtFinEnvio;
    }

    /**
     * @param dtFinEnvio Data final do envio da informação
     */
    public void setDtFinEnvio(Date dtFinEnvio) {
        if (dtFinEnvio != null) {
            this.dtFinEnvio = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(dtFinEnvio);
        }
    }

    /**
     * @return Data final da informação
     */
    public Date getDtFinInfo() {
        return dtFinInfo == null ? null : dtFinInfo.toGregorianCalendar().getTime();
    }

    /**
     * @param dtFinInfo Data final da informação
     */
    public void setDtFinInfo(Date dtFinInfo) {
        if (dtFinInfo != null) {
            this.dtFinInfo = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtFinInfo);
        }
    }

    public XMLGregorianCalendar getDtInitEnvio() {
        return dtInitEnvio;
    }

    /**
     * @param dtInitEnvio Data inicial do envio da informação
     */
    public void setDtInitEnvio(Date dtInitEnvio) {
        if (dtInitEnvio != null) {
            this.dtInitEnvio = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(dtInitEnvio);
        }
    }

    /**
     * @return Data inicial da informação
     */
    public Date getDtInitInfo() {
        return dtInitInfo == null ? null : dtInitInfo.toGregorianCalendar().getTime();
    }

    /**
     * @param dtInitInfo Data inicial da informação
     */
    public void setDtInitInfo(Date dtInitInfo) {
        if (dtInitInfo != null) {
            this.dtInitInfo = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtInitInfo);
        }
    }

    public Integer getFirstPage() {
        return firstPage;
    }

    public void setFirstPage(Integer firstPage) {
        this.firstPage = firstPage;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    /**
     * @return Página a ser obtida
     */
    public int getPage() {
        return page;
    }

    /**
     * @param page Página a ser obtida
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @return Quantidade máxima de elementos retornados em cada iteração
     */
    public Integer getQtMaxElement() {
        return qtMaxElement;
    }

    /**
     * @param qtMaxElement Quantidade máxima de elementos retornados em cada iteração
     */
    public void setQtMaxElement(Integer qtMaxElement) {
        this.qtMaxElement = qtMaxElement;
    }

    public MessageRetornoPLCotaComplexType getResponse() {
        return ret;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Invocação da operação ServicePLCota.consumir
     *
     * @return Registros de PL Cota. Se a propriedade downloadAllRecords for true, todos os
     * registros serão obtidos através de várias iterações.
     * <p>
     * @throws ConsumirFaultMsg
     */
    public MessageRetornoPLCotaComplexType invoke() throws ConsumirFaultMsg {
        Objects.requireNonNull(user, "Login e Password são obrigatórios");
        Objects.requireNonNull(getAmbiente(), "É necessário definir o ambiente (homologacao ou producao)");
        instantiateWS();

        //setReceiveTimeout(servicePLCota, Config.getInstance().getInteger(Config.CONNECTION_RECEIVE_TIMEOUT, 300000));
        setReceiveTimeout(servicePLCota, receiveTimeout);

        if (firstPage != null) {
            page = firstPage;
        } else {
            firstPage = page;
        }
        ret = null;
        if (firstPage.equals(lastPage)) {
            ret = invoke_once();
            return ret;
        } else {
            return invoke_all();
        }
    }

    /**
     * @return Indica se as iterações devem se repetir até que todos os registros sejam obtidos
     */
    public boolean isDownloadAllRecords() {
        return downloadAllRecords;
    }

    /**
     * @param downloadAllRecords Indica se as iterações devem se repetir até que todos os registros
     *                           sejam obtidos
     */
    public void setDownloadAllRecords(boolean downloadAllRecords) {
        this.downloadAllRecords = downloadAllRecords;
    }

    /**
     * Define a atuação do usuário, se provedor ou consumidor
     *
     * @param atuacao
     * @return
     */
    public WSConsumirPLCota setAtuacao(Atuacao atuacao) {
        this.atuacao = atuacao;
        return this;
    }

    public void setCallback(ConsumidorCallback callback) {
        this.callback = callback;
    }

    public void setCodSTIList(List<Integer> codSTIList) {
        this.codSTIList = codSTIList;
    }

    /**
     * Seta o WSListener, que redceberá algumas mensagens de notificação
     *
     * @param listener
     */
    public void setListener(WSListener listener) {
        if (this.listener == null || this.listener != listener) {
            this.listener = listener;
        } else {
            log.info("O WSListener foi mantido");
        }
    }

    public WSConsumirPLCota setReceiveTimeout(int receiveTimeout) {
        this.receiveTimeout = receiveTimeout;
        return this;
    }

    @Override public String toString() {
        //@formatter:off
        return "WSConsumirPLCota{"
                   + ((codSTIList != null) ? "codSTIList=" + codSTIList : "codSTI=" + codSTI)
                   + ", dtFinEnvio=" + dtFinEnvio
                   + ", dtInitEnvio=" + dtInitEnvio
                   + ", dtFinInfo=" + dtFinInfo
                   + ", dtInitInfo=" + dtInitInfo
                   + ", qtMaxElement=" + qtMaxElement
                   + ", firstPage=" + firstPage
                   + ", lastPage=" + lastPage
                   + ", downloadAllRecords=" + downloadAllRecords
                   + ", page=" + page
                   + ", status=" + status
                   + ", atuacao=" + atuacao + '}';
        //@formatter:on
    }

    @Override protected String getWsUriName() {
        return SERVICE_PLCOTA;
    }

    /**
     * Invoca a operação Consumir do Web Service PLCota. Se ocorrer erro diferente de PC.0137 ou
     * PC.0237, tenta até 3 vezes.
     *
     * @param req     Request
     * @param sWriter StringWriter utilizado pelo log do CXF
     * @param msg     StringWriter que recebe o tempo gasto para a execução do Web Service
     */
    private MessageRetornoPLCotaComplexType consumir(MessageConsultaPLCotaComplexType req, StringWriter sWriter,
        StringWriter msg) throws ConsumirFaultMsg {
        log.info("REQUEST: {}", this);
        MessageRetornoPLCotaComplexType consumir = null;
        boolean getOutFromThisLooping = false;
        for (int counter = 0; counter < 4 && !getOutFromThisLooping; counter++) {
            msg.getBuffer().setLength(0);
            Cronometro cron = Cronometro.start();
            try {
                log.info("Invocando o Web Service...");
                if (StringUtils.isNotBlank(System.getProperty("plcota.exception.mock"))) {
                    String faultCode = System.getProperty("plcota.exception.mock");
                    SOAPFault soapFault = SOAPFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createFault();
                    soapFault.setFaultString(faultCode);
                    soapFault.setFaultCode(new QName(SOAPConstants.URI_NS_SOAP_ENVELOPE, "MOCK"));
                    soapFault.setFaultActor("START AP");
                    throw new SOAPFaultException(soapFault);
                }
                consumir = servicePLCota.consumir(req);
                break;
            } catch (ConsumirFaultMsg | SOAPFaultException e) {
                log.info("Exceção lançada:" + e.getClass().getName());

                Throwable rootExc = ExceptionUtils.getRootCause(e);
                if (rootExc instanceof ConnectException) {
                    log.error("Erro de conexao ao invocar o Web Service");
                    throw new ApplicationException("Erro ao estabelecer uma conexão com os servidores do Sistema Galgo",
                        e);
                } else if (rootExc instanceof UnknownHostException) {
                    log.error("Servidor desconhecido: {}", e.getMessage());
                    throw new ApplicationException("Servidor desconhecido: " + rootExc.getMessage(), e);
                }

                String errMsg = StringUtils.trimToEmpty(e.getMessage());
                switch (errMsg) {
                    case PC_0137:
                        log.info(e.getMessage());
                        notify(WSListener.Event.AFTER_RESPONSE_RAW, "PC.0137");
                        getOutFromThisLooping = true;
                        break;
                    case PC_0237:
                        String notifyMsg = "######## SENHA INVALIDA #########";
                        log.warn("{}: {}", e.getMessage() + notifyMsg);
                        notify(WSListener.Event.AFTER_RESPONSE_RAW, notifyMsg);
                        throw new ApplicationException(PC_0237);
                    default:
                        log.error("{}\n}{}", e.getMessage(), sWriter.getBuffer());
                        throw e;
                }
/*
                if (!PC_0137.equals(e.getMessage())) {
                    log.error("{}\n}{}", e.getMessage(), sWriter.getBuffer());
                } else {
                    log.info(e.getMessage());
                    break;
                }
                if (PC_0237.equals(e.getMessage())) {
                    String notifyMsg = "######## SENHA INVALIDA #########";
                    log.warn("{}: {}", e.getMessage() + notifyMsg);
                    notify(WSListener.Event.AFTER_RESPONSE_RAW, notifyMsg);
                    throw new ApplicationException(PC_0237);
                }
*/
            } catch (Exception e) {
                log.error(null, e);
            } catch (Throwable e) {
                log.error(null, e);
                throw e;
            } finally {
                String elapsedTime = cron.stop().toString();
                log.info("Operação realizada em {}", elapsedTime);
                msg.append(elapsedTime);
            }
        }

        return consumir;
    }

    /**
     * Instancia a classe ServiceANBID
     */
    private void instantiateWS() {
        servicePLCota = GalgoServicesFactory.newPLCotaServiceInstance();
        setEndPoint((BindingProvider) servicePLCota);
    }

    /**
     * Consome todas as páginas, invocando a classe de Callback após cada página retornada
     *
     * @return Retorna o response da última página
     * @throws ConsumirFaultMsg
     */
    private MessageRetornoPLCotaComplexType invoke_all() throws ConsumirFaultMsg {
        page--;
        do {
            page++;
            if (lastPage != null && page > lastPage) {
                break;
            }
            log.info("page={}, lastPage={}", page, lastPage);
            MessageRetornoPLCotaComplexType lastResponse = invoke_once();

            if (lastResponse == null) {
                return ret;
            } else {
                ret = lastResponse;
                callback.execute(lastResponse);
            }

        } while (!ret.getPricRptV04().getMsgPgntn().isLastPgInd());

        return ret;
    }

    /**
     * Invoca o WS
     *
     * @return
     * @throws ConsumirFaultMsg
     */
    private MessageRetornoPLCotaComplexType invoke_once() throws ConsumirFaultMsg {

        WSHeader.buildHeader(servicePLCota, user);

        StringWriter sWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(sWriter);
        CXFUtils.turnOnClientLoggingFeature(servicePLCota, writer);

        MessageConsultaPLCotaComplexType req;

        if (codSTI != null) {
            //@formatter:off
            req = new MessageConsultaPLCotaComplexTypeBuilder(
                codSTI,
                dtInitInfo,
                dtFinInfo, dtInitEnvio,
                dtFinEnvio, page,
                status, atuacao,
                MSG_SENDER,
                qtMaxElement).build();
            //@formatter:on
        } else {
            //@formatter:off
            req = new MessageConsultaPLCotaComplexTypeBuilder(
                codSTIList,
                dtInitInfo,
                dtFinInfo, dtInitEnvio,
                dtFinEnvio, page,
                status, atuacao,
                MSG_SENDER,
                qtMaxElement).build();
            //@formatter:on
        }

        String notifyMsg = "Recuperando a página " + page;
        log.info(notifyMsg);
        notify(WSListener.Event.BEFORE_REQUEST, notifyMsg);

        StringWriter msg = new StringWriter();
        MessageRetornoPLCotaComplexType consumir = consumir(req, sWriter, msg);
        if (consumir == null) {
            notifyMsg = "Nenhuma informação retornada";
            log.info(notifyMsg);
            notify(WSListener.Event.AFTER_RESPONSE, "Nenhuma informação retornada");
        } else {
            notifyMsg = consumir.getQtElementCount() + " registro(s) retornado(s) em " + msg.toString();
            log.info(notifyMsg);
            notify(WSListener.Event.AFTER_RESPONSE_RAW, notifyMsg);
        }

        return consumir;
    }

    /**
     * Envia notificações ao listener WSListener registrado nesta classe
     *
     * @param notifyMsg
     */
    private void notify(WSListener.Event evt, String notifyMsg) {
        if (listener != null) {
            try {
                listener.notify(evt, notifyMsg);
            } catch (Throwable e) {
                log.error(null, e);
            }
        }
    }
}
