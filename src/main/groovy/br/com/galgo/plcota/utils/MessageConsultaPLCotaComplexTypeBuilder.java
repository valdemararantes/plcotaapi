package br.com.galgo.plcota.utils;

import br.com.stianbid.schemaplcota.BuscaObjetoComplexType;
import br.com.stianbid.schemaplcota.MessageConsultaPLCotaComplexType;
import br.com.stianbid.schemaplcota.ObjetoComplexType;
import br.com.stianbid.schemaplcota.ParametrosPLCotaComplexType;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.List;

/**
 * Classe construtora do objeto MessageConsultaPLCotaComplexType
 * <p>
 * Created by valdemar.arantes on 08/03/2016.
 */
public class MessageConsultaPLCotaComplexTypeBuilder {

    private Integer codSTI;
    private List<Integer> codSTIList;
    private XMLGregorianCalendar dtInitInfo;
    private XMLGregorianCalendar dtFinInfo;
    private XMLGregorianCalendar dtInitEnvio;
    private XMLGregorianCalendar dtFinEnvio;
    private int page;
    private Integer status;
    private Atuacao atuacao;
    private String msgSender;
    private Integer qtMaxElement;

    //@formatter:off
    public MessageConsultaPLCotaComplexTypeBuilder(
            List<Integer> codSTIList,
            XMLGregorianCalendar dtInitInfo,
            XMLGregorianCalendar dtFinInfo,
            XMLGregorianCalendar dtInitEnvio,
            XMLGregorianCalendar dtFinEnvio,
            int page,
            Integer status,
            Atuacao atuacao,
            String msgSender,
            Integer qtMaxElement) {
    //@formatter:on
        this.codSTIList = codSTIList;
        this.dtInitInfo = dtInitInfo;
        this.dtFinInfo = dtFinInfo;
        this.dtInitEnvio = dtInitEnvio;
        this.dtFinEnvio = dtFinEnvio;
        this.page = page;
        this.status = status;
        this.atuacao = atuacao;
        this.msgSender = msgSender;
        this.qtMaxElement = qtMaxElement;
    }

    //@formatter:off
    public MessageConsultaPLCotaComplexTypeBuilder(
            Integer codSTI,
            XMLGregorianCalendar dtInitInfo,
            XMLGregorianCalendar dtFinInfo,
            XMLGregorianCalendar dtInitEnvio,
            XMLGregorianCalendar dtFinEnvio,
            int page,
            Integer status,
            Atuacao atuacao,
            String msgSender,
            Integer qtMaxElement) {
    //@formatter:on
        this.codSTI = codSTI;
        this.dtInitInfo = dtInitInfo;
        this.dtFinInfo = dtFinInfo;
        this.dtInitEnvio = dtInitEnvio;
        this.dtFinEnvio = dtFinEnvio;
        this.page = page;
        this.status = status;
        this.atuacao = atuacao;
        this.msgSender = msgSender;
        this.qtMaxElement = qtMaxElement;
    }

    /**
     * @return Retorna uma instância de MessageConsultaPLCotaComplexType
     */
    //@formatter:off
    public MessageConsultaPLCotaComplexType build() {
    //@formatter:on

        MessageConsultaPLCotaComplexType req = new MessageConsultaPLCotaComplexType();
        ParametrosPLCotaComplexType params = new ParametrosPLCotaComplexType();

        if (codSTI != null) {
            BuscaObjetoComplexType buscaObj = new BuscaObjetoComplexType();
            addCodSTI(buscaObj, codSTI);
            params.setBuscaObjeto(buscaObj);
        }

        if (codSTIList != null && !codSTIList.isEmpty()) {
            BuscaObjetoComplexType buscaObj = new BuscaObjetoComplexType();
            for (Integer cod : codSTIList) {
                addCodSTI(buscaObj, cod);
            }
            params.setBuscaObjeto(buscaObj);
        }

        if (dtInitInfo != null) {
            params.setDtInicInfo(dtInitInfo);
        }

        if (dtFinInfo != null) {
            params.setDtFinInfo(dtFinInfo);
        }

        if (dtInitEnvio != null) {
            params.setDtInicEnvioCota(dtInitEnvio);
        }

        if (dtFinEnvio != null) {
            params.setDtFinEnvioCota(dtFinEnvio);
        }

        params.setPgNb(page);

        if (status != null) {
            params.setStatusInfo(status);
        }

        if (atuacao != null) {
            params.setAtuacao(atuacao.getCode());
        }

        req.setParametros(params);

        req.setIdMsgSender(msgSender);

        if (qtMaxElement != null) {
            req.setQtMaxElement(qtMaxElement);
        }

        return req;
    }

    private void addCodSTI(BuscaObjetoComplexType buscaObj, Integer cod) {
        ObjetoComplexType obj = new ObjetoComplexType();
        obj.setCdSti(cod);
        buscaObj.getObjeto().add(obj);
    }

}
