/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota.utils;

import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType;

/**
 * @author valdemar.arantes
 */
public class PLCotaUtils {

    public static MessageRetornoPLCotaComplexType join(MessageRetornoPLCotaComplexType op1,
                                                       MessageRetornoPLCotaComplexType op2) {
        if (op1 == null) {
            return op2;
        }
        if (op2 == null) {
            return op1;
        }
        MessageRetornoPLCotaComplexType ret = op2;
        op2.setQtElementCount(op2.getQtElementCount() + op1.getQtElementCount());
        op1.getPricRptV04().getPricValtnDtls().addAll(op2.getPricRptV04().getPricValtnDtls());
        op2.getPricRptV04().getPricValtnDtls().clear();
        op2.getPricRptV04().getPricValtnDtls().addAll(op1.getPricRptV04().getPricValtnDtls());

        return ret;
    }
}
