package br.com.galgo.plcota.utils;

import java.math.BigInteger;

/**
 * Define a atuação do usuário na informação de PLCota
 *
 * Created by valdemar.arantes on 10/03/2016.
 */
public enum Atuacao {
    PROVEDOR(1), CONSUMIDOR(2);

    private int code;

    private Atuacao(int code) {
        this.code = code;
    }

    public BigInteger getCode() {
        return BigInteger.valueOf(code);
    }

    public static Atuacao fromChar(String character) {
        if ("P".equalsIgnoreCase(character)) {
            return Atuacao.PROVEDOR;
        }
        if ("C".equalsIgnoreCase(character)) {
            return Atuacao.CONSUMIDOR;
        }
        throw new IllegalArgumentException("Caracter " + character + " inválido");
    }
}
