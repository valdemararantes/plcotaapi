/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota;

/**
 *
 * @author valdemar.arantes
 */
public interface WSListener {

    enum Event {BEFORE_REQUEST, AFTER_RESPONSE_RAW, AFTER_RESPONSE};
    public void notify(Event evt, String notifyMsg);

}
