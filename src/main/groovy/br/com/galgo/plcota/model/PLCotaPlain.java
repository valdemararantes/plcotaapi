/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author valdemar.arantes
 */
public class PLCotaPlain {

    private final String cdSti;
    private final BigDecimal pl;
    private final BigDecimal cota;
    private final BigDecimal qtdCotas;
    private final Integer qtdCotistas;
    private final BigDecimal aplicacao;
    private final BigDecimal resgate;
    private final BigDecimal comeCotas;
    private final boolean reenvio;
    private final Date data;
    private final String usuario;

    public PLCotaPlain(String cdSti, BigDecimal pl, BigDecimal cota, BigDecimal qtdCotas,
            Integer qtdCotistas, BigDecimal aplicacao, BigDecimal resgate, BigDecimal comeCotas,
            boolean reenvio, Date data, String usuario) {
        this.cdSti = cdSti;
        this.pl = pl;
        this.cota = cota;
        this.qtdCotas = qtdCotas;
        this.qtdCotistas = qtdCotistas;
        this.aplicacao = aplicacao;
        this.resgate = resgate;
        this.comeCotas = comeCotas;
        this.reenvio = reenvio;
        this.data = data;
        this.usuario = usuario;
    }

    public BigDecimal getAplicacao() {
        return aplicacao;
    }

    public BigDecimal getResgate() {
        return resgate;
    }

    public BigDecimal getComeCotas() {
        return comeCotas;
    }

    public boolean isReenvio() {
        return reenvio;
    }

    public BigDecimal getQtdCotas() {
        return qtdCotas;
    }

    public Integer getQtdCotistas() {
        return qtdCotistas;
    }

    public String getCdSti() {
        return cdSti;
    }

    public BigDecimal getPl() {
        return pl;
    }

    public BigDecimal getCota() {
        return cota;
    }

    public Date getData() {
        return data;
    }

    public String getUsuario() {
        return usuario;
    }

}
