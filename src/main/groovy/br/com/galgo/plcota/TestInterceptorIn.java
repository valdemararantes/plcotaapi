package br.com.galgo.plcota;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestInterceptorIn extends AbstractSoapInterceptor {

    private static final Logger log = LoggerFactory.getLogger(TestInterceptorIn.class);

    public TestInterceptorIn() {
        super(Phase.PRE_MARSHAL);
    }

    @Override
    public void handleMessage(SoapMessage message) throws Fault {
        log.debug("message={}", message);
    }

    @Override
    public void handleFault(SoapMessage message) {
        super.handleFault(message);
        log.debug("message={}", message);
    }

    
}
