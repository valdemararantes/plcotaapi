/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota;

import br.com.galgo.galgowsgenerated.GalgoServicesFactory;
import br.com.stianbid.schemaplcota.BuscaObjetoComplexType;
import br.com.stianbid.schemaplcota.MessageConsultaPLCotaComplexType;
import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType;
import br.com.stianbid.schemaplcota.ObjetoComplexType;
import br.com.stianbid.schemaplcota.ParametrosPLCotaComplexType;
import br.com.stianbid.serviceplcota.ConsumirFaultMsg;
import br.com.stianbid.serviceplcota.ServicePLCota;
import com.galgo.cxfutils.CXFUtils;
import com.galgo.cxfutils.sec.WSHeader;
import com.galgo.cxfutils.ws.AmbienteEnum;
import com.galgo.cxfutils.ws.WS;
import com.galgo.utils.XMLGregorianCalendarConversionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * Classe que invoca o Consumir PLCota fatiando o tempo. O tempo será fatiado apenas se as datas de
 * envio inicial e final forem preenchidas.
 *
 * @author valdemar.arantes
 */
public class WSConsumirPLCotaSplitting extends WS {

    public static final Integer STATUS_ENVIADA = 1;
    public static final Integer STATUS_REENVIADA = 2;
    public static final Integer STATUS_CANCELADA_MAN = 3;
    public static final Integer STATUS_CANCELADA_AUT = 4;
    public static final Integer STATUS_REJEITADA = 5;
    private static final String SERVICE_PLCOTA = "/ServicePLCota";
    private static final String MSG_SENDER = "PLCota_GlgApp";
    private static final Logger log = LoggerFactory.getLogger(WSConsumirPLCotaSplitting.class);
    private static final Logger logXML = LoggerFactory.getLogger(WSConsumirPLCotaSplitting.class.
        getName() + ".XML");
    private static ServicePLCota servicePLCota;
    private XMLGregorianCalendar dtFinEnvio;
    private XMLGregorianCalendar dtInitEnvio;
    private XMLGregorianCalendar dtFinInfo;
    private XMLGregorianCalendar dtInitInfo;
    private Integer codSTI;
    private Integer qtMaxElement;
    private Integer firstPage;
    private Integer lastPage;
    private boolean downloadAllRecords = false;
    private int page = 1;
    private Integer status;
    private MessageRetornoPLCotaComplexType ret;
    private int timeout = 600_000;

    /**
     * Recupera o ambiente (homologação ou produção) e guarda em um campo
     */
    public WSConsumirPLCotaSplitting(AmbienteEnum ambiente) {
        setAmbiente(ambiente);
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param qtMaxElement
     */
    public WSConsumirPLCotaSplitting(AmbienteEnum amb, Integer codSTI, XMLGregorianCalendar dtInitInfo,
        XMLGregorianCalendar dtFinInfo, Integer qtMaxElement) {
        this(amb);
        this.codSTI = codSTI;
        this.dtInitInfo = dtInitInfo;
        this.dtFinInfo = dtFinInfo;
        this.qtMaxElement = qtMaxElement;
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param dtInitEnvio
     * @param dtFinEnvio
     * @param qtMaxElement
     */
    public WSConsumirPLCotaSplitting(AmbienteEnum amb, Integer codSTI, Date dtInitInfo, Date dtFinInfo,
        Date dtInitEnvio, Date dtFinEnvio, Integer qtMaxElement, Integer firstPage, Integer lastPage) {
        this(amb);
        this.codSTI = codSTI;
        setDtInitInfo(dtInitInfo);
        setDtFinInfo(dtFinInfo);
        setDtInitEnvio(dtInitEnvio);
        setDtFinEnvio(dtFinEnvio);
        log.debug("dtInitEnvio={}, dtFinEnvio={}", dtInitEnvio, dtFinEnvio);
        this.qtMaxElement = qtMaxElement;
        this.firstPage = firstPage;
        this.lastPage = lastPage;
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param dtInitEnvio
     * @param dtFinEnvio
     * @param qtMaxElement
     * @param firstPage
     * @param lastPage
     * @param status       Procure utilizar as constantes definidas nesta classe
     */
    public WSConsumirPLCotaSplitting(AmbienteEnum amb, Integer codSTI, Date dtInitInfo, Date dtFinInfo,
        Date dtInitEnvio, Date dtFinEnvio, Integer qtMaxElement, Integer firstPage, Integer lastPage, Integer status,
        Long splittingTime) {
        this(amb, codSTI, dtInitInfo, dtFinInfo, dtInitEnvio, dtFinEnvio, qtMaxElement, firstPage, lastPage);
        this.status = status;
    }

    /**
     * @param codSTI
     * @param dtInitInfo
     * @param dtFinInfo
     * @param qtMaxElement
     */
    public WSConsumirPLCotaSplitting(AmbienteEnum amb, Integer codSTI, Date dtInitInfo, Date dtFinInfo,
        Integer qtMaxElement) {
        this(amb, codSTI, XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtInitInfo),
            XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtFinInfo), qtMaxElement);
    }

    /**
     * @return Código STI do Fundo de Investimento
     */
    public Integer getCodSTI() {
        return codSTI;
    }

    /**
     * @param codSTI Código STI do Fundo de Investimento
     */
    public void setCodSTI(Integer codSTI) {
        this.codSTI = codSTI;
    }

    /**
     * @return Data final da informação
     */
    public Date getDtFinInfo() {
        return dtFinInfo == null ? null : dtFinInfo.toGregorianCalendar().getTime();
    }

    /**
     * @param dtFinInfo Data final da informação
     */
    public void setDtFinInfo(Date dtFinInfo) {
        if (dtFinInfo != null) {
            this.dtFinInfo = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtFinInfo);
        }
    }

    /**
     * @return Data inicial da informação
     */
    public Date getDtInitInfo() {
        return dtInitInfo == null ? null : dtInitInfo.toGregorianCalendar().getTime();
    }

    /**
     * @param dtInitInfo Data inicial da informação
     */
    public void setDtInitInfo(Date dtInitInfo) {
        if (dtInitInfo != null) {
            this.dtInitInfo = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar_Date(dtInitInfo);
        }
    }

    public Integer getFirstPage() {
        return firstPage;
    }

    public void setFirstPage(Integer firstPage) {
        this.firstPage = firstPage;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    /**
     * @return Página a ser obtida
     */
    public int getPage() {
        return page;
    }

    /**
     * @param page Página a ser obtida
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @return Quantidade máxima de elementos retornados em cada iteração
     */
    public Integer getQtMaxElement() {
        return qtMaxElement;
    }

    /**
     * @param qtMaxElement Quantidade máxima de elementos retornados em cada iteração
     */
    public void setQtMaxElement(Integer qtMaxElement) {
        this.qtMaxElement = qtMaxElement;
    }

    public MessageRetornoPLCotaComplexType getResponse() {
        return ret;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Invocação da operação ServicePLCota.consumir
     *
     * @return Registros de PL Cota. Se a propriedade downloadAllRecords for true, todos os
     * registros serão obtidos através de várias iterações.
     * @throws ConsumirFaultMsg
     */
    public MessageRetornoPLCotaComplexType invoke() throws ConsumirFaultMsg {
        instantiateWS();
        setReceiveTimeout(servicePLCota, timeout);
        page = firstPage;
        ret = null;
        if (firstPage.equals(lastPage)) {
            ret = invoke_once();
            return ret;
        } else {
            return invoke_all();
        }
    }

    /**
     * @return Indica se as iterações devem se repetir até que todos os registros sejam obtidos
     */
    public boolean isDownloadAllRecords() {
        return downloadAllRecords;
    }

    /**
     * @param downloadAllRecords Indica se as iterações devem se repetir até que todos os registros
     *                           sejam obtidos
     */
    public void setDownloadAllRecords(boolean downloadAllRecords) {
        this.downloadAllRecords = downloadAllRecords;
    }

    /**
     * @param dtFinEnvio Data final do envio da informação
     */
    public void setDtFinEnvio(Date dtFinEnvio) {
        if (dtFinEnvio != null) {
            this.dtFinEnvio = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(dtFinEnvio);
        }
    }

    /**
     * @param dtInitEnvio Data inicial do envio da informação
     */
    public void setDtInitEnvio(Date dtInitEnvio) {
        if (dtInitEnvio != null) {
            this.dtInitEnvio = XMLGregorianCalendarConversionUtil.asXMLGregorianCalendar(dtInitEnvio);
        }
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public String toString() {
        return "WSConsumirPLCota{" + "codSTI=" + codSTI + ", dtFinEnvio=" + dtFinEnvio + ", dtInitEnvio=" +
            dtInitEnvio + ", dtFinInfo=" + dtFinInfo + ", dtInitInfo=" + dtInitInfo + ", qtMaxElement=" +
            qtMaxElement + ", firstPage=" + firstPage + ", lastPage=" + lastPage + ", downloadAllRecords=" +
            downloadAllRecords + ", page=" + page + ", status=" + status + '}';
    }

    @Override
    protected String getWsUriName() {
        return SERVICE_PLCOTA;
    }

    /**
     * Instancia a classe ServiceANBID
     */
    private void instantiateWS() {
        servicePLCota = GalgoServicesFactory.newPLCotaServiceInstance();
        setEndPoint((BindingProvider) servicePLCota);
        //        Client cxfClient = ClientProxy.getClient(servicePLCota);
        //        cxfClient.getOutInterceptors().add(new TestInterceptorOut());
        //        cxfClient.getInInterceptors().add(new TestInterceptorIn());
    }

    /**
     * @return Todos os registros realizando várias iterações, se necessário
     * @throws ConsumirFaultMsg
     */
    private MessageRetornoPLCotaComplexType invoke_all() throws ConsumirFaultMsg {
        page--;
        do {
            page++;
            if (lastPage != null && page > lastPage) {
                break;
            }
            log.info("page={}, lastPage={}", page, lastPage);
            MessageRetornoPLCotaComplexType _ret = invoke_once();

            if (_ret == null) {
                return ret;
            }

        } while (!ret.getPricRptV04().getMsgPgntn().isLastPgInd());

        return ret;
    }

    private MessageRetornoPLCotaComplexType invoke_once() throws ConsumirFaultMsg {
        WSHeader.buildHeader(servicePLCota, user);

        StringWriter sWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(sWriter);
        CXFUtils.turnOnClientLoggingFeature(servicePLCota, writer);

        MessageConsultaPLCotaComplexType req = new MessageConsultaPLCotaComplexType();
        ParametrosPLCotaComplexType params = new ParametrosPLCotaComplexType();

        if (codSTI != null) {
            BuscaObjetoComplexType buscaObj = new BuscaObjetoComplexType();
            ObjetoComplexType obj = new ObjetoComplexType();
            obj.setCdSti(codSTI);
            buscaObj.getObjeto().add(obj);
            params.setBuscaObjeto(buscaObj);
        }

        if (dtInitInfo != null) {
            params.setDtInicInfo(dtInitInfo);
        }

        if (dtFinInfo != null) {
            params.setDtFinInfo(dtFinInfo);
        }

        if (dtInitEnvio != null) {
            params.setDtInicEnvioCota(dtInitEnvio);
        }

        if (dtFinEnvio != null) {
            params.setDtFinEnvioCota(dtFinEnvio);
        }

        params.setPgNb(page);

        if (status != null) {
            params.setStatusInfo(status);
        }

        req.setParametros(params);
        req.setIdMsgSender(MSG_SENDER);
        if (qtMaxElement != null) {
            req.setQtMaxElement(qtMaxElement);
        }

        try {
            String notifyMsg = "Recuperando a página " + page;
            log.info(notifyMsg);

            long startTime = System.nanoTime();
            MessageRetornoPLCotaComplexType consumir = servicePLCota.consumir(req);
            double elapsedTime = (System.nanoTime() - startTime) / Math.pow(10, 9);

            logXML.debug("\n{}", sWriter.getBuffer());

            notifyMsg = consumir.getQtElementCount() + " registro(s) retornado(s) em " + elapsedTime + " seg";
            log.info(notifyMsg);

            return consumir;
        } catch (ConsumirFaultMsg e) {
            log.error("{}\n}{}", e.getMessage(), sWriter.getBuffer());
            throw e;
        } catch (SOAPFaultException e) {
            log.error("{}\n}{}", e.getMessage(), sWriter.getBuffer());
            throw e;
        }
    }
}
